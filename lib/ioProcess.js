"use strict";

var u = require("./utils");
var childProcess = require("child_process");

var spawn = childProcess.spawn;
var spawnSync = childProcess.spawnSync;

var INPUT_OPTIONS = {stdio: ["pipe", "ignore", "ignore"]};
var OUTPUT_OPTIONS = {stdio: ["ignore", "pipe", "ignore"]};
var EXISTS_OPTIONS = {stdio: "ignore", shell: true};

/*===================================================== Exports  =====================================================*/

exports.input = startInputProcess;
exports.output = startOutputProcess;
exports.exists = exists;

/*==================================================== Functions  ====================================================*/

function startInputProcess(cmd, args, input, cb) {
  var child = spawn(cmd, args, INPUT_OPTIONS);
  cb = u.once(cb);
  child.on("error", cb);
  child.on("close", function (code) {
    if (code) { cb(new Error(cmd + " did not shut down properly. Exit code: " + code)); } else { cb(null, true); }
  });
  child.stdin.end(input);
  return child;
}

function startOutputProcess(cmd, args, cb) {
  var child = spawn(cmd, args, OUTPUT_OPTIONS);
  cb = u.once(cb);
  child.on("error", cb);
  var data = "";
  child.stdout.on("data", function (chunk) { data += chunk; });
  child.on("close", function (code) {
    if (code) { cb(new Error(cmd + " did not shut down properly. Exit code: " + code)); } else { cb(null, data); }
  });
  return child;
}

function exists(cmd) { return spawnSync("which", [cmd], EXISTS_OPTIONS).status === 0; }
