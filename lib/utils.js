"use strict";

/*===================================================== Exports  =====================================================*/

exports.once = once;

/*==================================================== Functions  ====================================================*/

function once(cb) {
  var result, waiting = true;
  return function () {
    if (waiting) {
      waiting = false;
      result = cb.apply(this, arguments);
    }
    return result;
  };
}
